## News without distractions
    goodnews.py [-h] [-f] url

    positional arguments:
      url         URL to the article

    optional arguments:
      -h, --help  show this help message and exit
      -f, --fun   include XKCD substitutions to make reading the news more
                  pleasant

Only use fully qualified URLs (with leading `http(s)://www.` etc)

### Requirements
 - Python 3.6+
 - [requests](http://docs.python-requests.org/en/master/)
 - [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)


### Sites supported:
 - Economist, The [(www.economist.com)](http://www.economist.com)
 - Gizmodo [(www.gizmodo.com)](http://www.gizmodo.com/)
 - New York Times, The [(www.nytimes.com)](http://www.nytimes.com/)
 - Straits Times, The [(www.straitstimes.com)](http://www.straitstimes.com/)
 - Washington Post [(www.washingtonpost.com)](https://www.washingtonpost.com)

More sites to be added as and when I come across them.

### Acknowledgements
 - [xkcd](xkcd.com)
 - [clickyotomy](https://github.com/clickyotomy/xkcd-substitutions) for the list of substitutions