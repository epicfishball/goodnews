#-*- coding: utf-8 -*-
"""
Usage:
    python goodnews.py -h


Todo:
    * generate html tempfile for display in browser
"""


import os
import sys
from textwrap import wrap
import argparse
from bs4 import BeautifulSoup
import requests


LINE = '_' * 76

def pretty_print(para_list, url):
    options = {
        'width':                78,
        'initial_indent':       '',
        'subsequent_indent':    '',
        'break_on_hyphens':     True,
        'drop_whitespace':      True,
        'replace_whitespace':   False
    }
    global fun
    
    cols, lines = os.get_terminal_size()

    para_list.insert(0, LINE)
    para_list += [LINE, '\n']

    for para in para_list:
        if not para:
            continue
        para = para.replace('—', '--')
        if fun:
            para = sub_with_caps(para)
        print(end='\n')
        for line in wrap(para, **options):
            # line = repr(line)
            # margin = margin.replace(' ', '^')
            left_aligned = f'{line:<78}'
            if line.strip():
                print(f'{left_aligned:^{cols-4}}')

    print(f'  Link:\n    {url}')

def sub_with_caps(text):
    global substitutions
    for find_me, new in substitutions.items():
        replacement = new
        found = re.search(r'\b({})\b'.format(find_me), 
                          text, 
                          re.IGNORECASE)
        if not found:
            continue
        match = found.group(0)
        if match[0].isupper():
            replacement = replacement[0].upper() + replacement[1:]
        if match.istitle():
            replacement = replacement.title()
        elif match.islower():
            replacement = replacement.lower()
        elif match.isupper:
            replacement = replacement.upper()
        text = re.sub(r'\b({})\b'.format(find_me), 
                      replacement, 
                      text, 
                      flags=re.IGNORECASE)
        #print('{} -> {}'.format(find_me, replacement))
    return text

def getdomain(url):
    return url.split('/')[2]

def getcontent(url):
    domain_info = {
        'www.washingtonpost.com': get_washpost,
        'www.straitstimes.com': get_straitstimes,
        'www.nytimes.com': get_newyorktimes,
        'www.gizmodo.com': get_gizmodo,
        #'wsj.com': (get_wsj, True)
        'www.economist.com': get_economist,
    }
    getter = need_ref = headers = None
    domain = getdomain(url)

    if domain not in domain_info:
        raise NotImplementedError
    try:
        getter, need_ref = domain_info[domain]
        if need_ref:
            headers = {'referer': 'https://www.twitter.com'}
    except TypeError:  # if non-tuple entry
        getter = domain_info[domain]
        

    r = requests.get(url, headers=headers)
    assert(r.ok)
    soup = BeautifulSoup(r.text, 'html.parser')
    return getter(soup)



def get_washpost(soup):
    cols, lines = os.get_terminal_size()
    headline = soup.find('h1', itemprop='headline').text

    paras = [headline + '\n' + '_' * len(headline)]
    article = soup.find('article', itemprop='articleBody')
    for x in article.find_all('p', class_=None):
        paras.append(x.text.strip())
    return paras


def get_straitstimes(soup):
    headline = soup.find('h1', itemprop='headline').text
    paras = [headline + '\n' + LINE]
    article = soup.find('div', itemprop='articleBody')
    for x in article.find_all('p', class_=None):
        paras.append(x.text.strip())
    return paras


def get_newyorktimes(soup):
    headline = soup.find('h1', itemprop='headline').text
    paras = [headline + '\n' + LINE]
    article = soup.find('article', id='story')
    for x in article.find_all('p', class_='story-body-text'):
        paras.append(x.text.strip())
    return paras


def get_gizmodo(soup):
    headline = soup.find('h1', class_='headline').text
    paras = [headline + '\n' + LINE]
    article = soup.find('article', class_='post')
    for x in article.find_all('p', class_=None):
        paras.append(x.text.strip())
    return paras


def get_economist(soup):
    global article
    headlines = [e.text for e in 
        soup.find('h1', class_='flytitle-and-title__body').find_all('span')]
    subtitle = soup.find('p', class_='blog-post__rubric').text
    paras = [*headlines, subtitle + '\n' + LINE]
    article = soup.find('div', class_='blog-post__text')
    for x in article.find_all('p', class_=None):
        if 'blog-post__text' not in x.parent.attrs['class']:
            continue
        paras.append(x.text.strip())
    return paras






if __name__ == '__main__':
    global fun
    global substitutions
    argp = argparse.ArgumentParser(description=('News without distractions.'))
    argp.add_argument('url', help='url to the article')
    argp.add_argument('-f', '--fun', 
                      action='store_true', 
                      help=('include XKCD substitutions to make reading the '
                            'news more pleasant'))
    namespace = argp.parse_args()
    fun = namespace.fun
    if fun:
        import re
        import json
        with open('substitutions.json', 'r', encoding='utf-8') as f:
            substitutions = json.load(f)

    try:
        url = namespace.url
        paras = getcontent(url)
        pretty_print(paras, url)

    except AssertionError:
        print('error: bad url?')

    except NotImplementedError:
        print("error: that url isn't supported. sorry!")
